Software for reading values from Ozone UV-100 analyzer. The communication goes via serial-USB interface.

## How to use it

Download `Release`, extract zip and run the program.
 `Select port` and see readings. Use `Reset data` to set an initial time and `Save data` when you are happy with the result. It saves to `*.csv` format.

When the connection is interrupted (eg. unplugged cable), the program tries to connect again and continue. If that does not work, `Refresh ports` to see if the port did not change.

![1684521183417](image/README/1684521183417.png)

## Build prerequisites

##### **Windows**

1. Download the newest [Python 3](https://www.python.org/downloads/) and do not forget to add it to PATH during the installation.
2. In the cmd: `pip install dearpygui numpy serial auto-py-to-exe`
3. Run `auto-py-to-exe.exe` (it should be located in the script folder in the python installation folder)

##### **Linux (everything in terminal)**

1. `apt-get install python3 python3-tk`
2. `pip install dearpygui numpy serial auto-py-to-exe`
3. `auto-py-to-exe`

## Compilation

The auto-py-to-exe opens tab in the web browser:

1. Script location is the `OzoneUV100.py` file.
2. I recommend choosing `One File` option and `Window Based`.
3. You can add program icon.
4. Choose the `Output Directory` under `Settings`.
5. Hit `CONVERT .PY TO .EXE`

![1684520373517](image/README/1684520373517.png)
