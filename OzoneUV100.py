import dearpygui.dearpygui as dpg
import threading
import numpy as np
from tkinter import filedialog
import sys
import glob
import serial
from datetime import datetime, timedelta
import copy as copy


class SerialPort:
    def __init__(self):
        self.__SerialParameters = []
        self.__Ozone_ppm = []
        self.__Delta_time = []
        self.__Init_time = []
        self.SerialPortList = self.__serial_ports()
        self.__time_change_object = Change_indicator()
        self.__thread_active = True
        self.labelX = ["Time [s]"]
        self.labelY = ["Ozone [ppm]"]

    def stop_threads(self):
        self.__thread_active = False

    def __serial_ports(self):
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def port_refresh(self):
        self.SerialPortList = self.__serial_ports()
        dpg.configure_item("port_selector", items=self.SerialPortList)
        print(self.SerialPortList)

    def setSerialParameters(self):
        try:
            self.__SerialParameters = serial.Serial(
                dpg.get_value("port_selector"), 9600, timeout=1)
        except (OSError, serial.SerialException):
            print("COM not there")

    def __serialRead(self):
        if self.__SerialParameters:
            try:
                line = self.__SerialParameters.readline()
            except (OSError, serial.SerialException):
                line = []
                self.setSerialParameters()
            if line and len(line) > 32:
                serial_out = line.decode().strip().replace('\x00', '').split(",")
                return serial_out
            else:
                return False

    def __stripOzone(self, ser_input):
        self.__Ozone_ppm.append(float(ser_input[0]))

    def __stripTime(self, ser_input):
        stripped_time = datetime.strptime(
            ser_input[5] + " " + ser_input[6], '%d/%m/%y %H:%M:%S')
        if not self.__Delta_time:
            self.__Init_time = stripped_time
        self.__Delta_time.append(
            (stripped_time - self.__Init_time).total_seconds())

    def serialRun(self):
        while self.__thread_active:
            SerialOut = self.__serialRead()
            if SerialOut:
                self.__stripOzone(SerialOut)
                self.__stripTime(SerialOut)
            if self.__time_change_object.getStoredObject() != self.__Delta_time:
                self.__time_change_object.setStoredObject(self.__Delta_time)
                dpg.set_value(
                    "series_tag", [list(self.__Delta_time), list(self.__Ozone_ppm)])
                dpg.fit_axis_data("x_axis")
                dpg.fit_axis_data("y_axis")
        #self.__SerialParameters.close()

    def data_reset(self):
        self.__Delta_time = []
        self.__Ozone_ppm = []

    def data_save(self):
        np.savetxt(filedialog.asksaveasfilename(defaultextension=".csv", filetypes=[(("Text files"), "*.csv")]),
                   [p for p in zip(self.labelX + self.__Delta_time,
                                   self.labelY + self.__Ozone_ppm)],
                   delimiter=";", fmt="%s")


class Change_indicator:
    def __init__(self):
        self.__Object = []

    def getStoredObject(self):
        return self.__Object

    def setStoredObject(self, seposeru):
        self.__Object = copy.copy(seposeru)


class MyApp:
    def __init__(self):
        self.__serial0 = SerialPort()

    def __startup(self):
        dpg.create_context()
        dpg.create_viewport(title="Ozone UV-100", width=1280, height=720)
        dpg.setup_dearpygui()
        dpg.show_viewport()

    def __gui_body(self):
        with dpg.window(label="##", tag="win"):
            with dpg.group(horizontal=True):
                dpg.add_button(label="Refresh ports",
                               callback=self.__serial0.port_refresh)
                dpg.add_combo(label="##",
                              tag="port_selector",
                              default_value="Select port",
                              items=self.__serial0.SerialPortList,
                              callback=self.__serial0.setSerialParameters,
                              width=100)
            with dpg.group(horizontal=True):
                dpg.add_button(label="Reset data",
                               callback=self.__serial0.data_reset)
                dpg.add_button(label="Save data",
                               callback=self.__serial0.data_save)

            with dpg.plot(label="##", height=-1, width=-1):
                # optionally create legend
                # dpg.add_plot_legend()

                # REQUIRED: create x and y axes
                dpg.add_plot_axis(
                    dpg.mvXAxis, label=self.__serial0.labelX[0], tag="x_axis")
                dpg.add_plot_axis(
                    dpg.mvYAxis, label=self.__serial0.labelY[0], tag="y_axis")

                # add series, the tag is used for update
                dpg.add_scatter_series(x=list([]), y=list([]),
                                       label="Ozone", parent="y_axis",
                                       tag="series_tag")
        # make background window
        dpg.set_primary_window("win", True)

    def __process_start(self):
        thread_serial = threading.Thread(target=self.__serial0.serialRun)
        thread_serial.start()
        # dpg.show_imgui_demo()
        dpg.start_dearpygui()

    def __cleanup(self):
        dpg.destroy_context()
        self.__serial0.stop_threads()

    def app_run(self):
        self.__startup()
        self.__gui_body()
        self.__process_start()
        self.__cleanup()


app = MyApp()
app.app_run()
